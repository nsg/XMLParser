package com.jack.xmlparser.iui;

import java.io.InputStream;
import java.util.List;

public interface BaseParser {
	
	public List<Object> parserToObject(InputStream is);
	
	public String parserToXml(List<Object> list);
}
